package com.example.resterauntbackend.user.service;

import com.example.resterauntbackend.user.model.User;
import com.example.resterauntbackend.user.repo.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAllUser(){
        return userRepository.findAll();
    }
    public Optional<User> login(String email, String password) { return userRepository.findByEmailAndPassword(email, password); }
}
