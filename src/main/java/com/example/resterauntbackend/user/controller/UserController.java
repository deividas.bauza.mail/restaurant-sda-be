package com.example.resterauntbackend.user.controller;

import com.example.resterauntbackend.user.model.LoginAttempt;
import com.example.resterauntbackend.user.model.User;
import com.example.resterauntbackend.user.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("user")
    List<User> getAll(){
        return userService.findAllUser();
    }

    @GetMapping("login")
    User login(@Valid LoginAttempt login) throws Exception {
        return userService.login(login.getEmail(), login.getPassword()).orElseThrow(
                () -> new Exception("Neteisingi prisijungimo duomenys")
        );
    }
}
