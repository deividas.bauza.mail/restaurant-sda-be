package com.example.resterauntbackend.user.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class LoginAttempt {

    @Length(min=7, message = "Password field is too short")
    private String password;

    @Email(message = "Invalid email format")
    @NotEmpty(message = "Mano custom zinute")
    private String email;
}
