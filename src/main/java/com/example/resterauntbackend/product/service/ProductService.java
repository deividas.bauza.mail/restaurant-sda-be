package com.example.resterauntbackend.product.service;

import com.example.resterauntbackend.product.model.Product;
import com.example.resterauntbackend.product.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getProducts(String category) {
        if (category == null) {
            return getAllProducts();
        }
        else {
            return getProductsByCategory(category);
        }
    }

    private List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    private List<Product> getProductsByCategory(String category) {
        return productRepository.getAllByCategory(category);
    }



}
