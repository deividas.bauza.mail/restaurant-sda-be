package com.example.resterauntbackend.product.controller;

import com.example.resterauntbackend.product.model.Product;
import com.example.resterauntbackend.product.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("product")
    public List<Product> findProducts(String category) {
        return productService.getProducts(category);
    }

}
