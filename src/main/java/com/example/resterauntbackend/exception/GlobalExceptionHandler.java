package com.example.resterauntbackend.exception;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    List<FieldErrorMessage> exceptionHandler(BindException e) {
        return e.getBindingResult().getFieldErrors()
                .stream()
                .map(fieldError -> new FieldErrorMessage(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());
    }

//
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @org.springframework.web.bind.annotation.ExceptionHandler
//    ErrorMessage exceptionHandler(CancellationException e) {
//        return new ErrorMessage("400", "Can only cancel on the same day before 00:00");
//    }
//
//
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @org.springframework.web.bind.annotation.ExceptionHandler
//    ErrorMessage exceptionHandler(InvalidFormatException e) {
//        String message = e.getMessage().contains("paymentType") ? "Invalid payment type entered"
//                : e.getMessage().contains("Currency") ? "This currency is not supported yet."
//                : e.getMessage();
//        return new ErrorMessage("400", message);
//    }
//
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @org.springframework.web.bind.annotation.ExceptionHandler
//    ErrorMessage exceptionHandler(CustomException e) {
//        if (loggedExceptions.contains(e.getClass().getSimpleName()))
//            loggingService.save(Log.of(e.getMessage(), HttpStatus.BAD_REQUEST));
//        return new ErrorMessage("400", e.getMessage());
//    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    ErrorMessage exceptionHandler(Exception e) {
        return new ErrorMessage(HttpStatus.BAD_REQUEST, e.getMessage());
    }

}

