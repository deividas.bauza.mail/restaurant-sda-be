drop table if exists delivery_details;
create table if not exists  delivery_details
(
    id           int auto_increment primary key not null,
    address      varchar(255) not null,
    city         varchar(50)  not null,
    phone_number varchar(50) not null,
    user_id int not null,

    constraint fk_delivery_details_id foreign key (user_id) references delivery_details(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    constraint delivery_details_id_uindex
        unique (id)
);
drop table if exists user;
create table if not exists user
(
    id           int auto_increment primary key,
    email        varchar(100) not null,
    password     varchar(50) not null,
    first_name   varchar(100) not null,
    last_name    varchar(100) not null,

    constraint user_id_uindex
    unique (id)
    );
drop table if exists product;
create table if not exists product
(
    id int auto_increment primary key ,
    name varchar(50) not null,
    price float not null,
    description varchar(255) not null,
    category varchar(50) not null,

    constraint product_id_uinsex
    unique (id)
);
