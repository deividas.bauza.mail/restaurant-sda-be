truncate table product;
insert into product(id, name, price, description, category)
values(1, 'steak', 20.00, 'steak with potatoes', 'business launch'),
(2, 'sandwiches', 5.00, 'sandwiches with chicken', 'food delivery'),
(3, 'coca-cola', 1.50, 'coca-cola', 'drinks');